<?php

namespace Drupal\linked_entity_reference\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;
use Drupal\linked_entity_reference\Plugin\Field\FieldType\LinkedEntityReference;

/**
 * Class LinkedEntityReferenceEntity.
 *
 * @package Drupal\linked_entity_reference\Plugin\Field\FieldFormatter
 *
 * @FieldFormatter(
 *   id = "linked_entity_reference_entity_view",
 *   label = @Translation("Rendered entity"),
 *   description = @Translation("Display the referenced entities rendered by entity_view()."),
 *   field_types = {
 *     "linked_entity_reference"
 *   }
 * )
 */
class LinkedEntityReferenceEntity extends EntityReferenceEntityFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    if (!$items->isEmpty()) {
      foreach ($items as $delta => $item) {
        if (!empty($elements[$delta]) && $item instanceof LinkedEntityReference && !empty($item->uri)) {
          $elements[$delta] = [
            '#type' => 'link',
            '#title' => $elements[$delta],
            '#url' => $item->getUrl(),
            '#options' => $item->getUrl()->getOptions(),
          ];
        }
      }
    }

    return $elements;
  }

}
