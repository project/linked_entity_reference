<?php

namespace Drupal\linked_entity_reference\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;

/**
 * Class LinkedEntityReferenceLabel
 *
 * @package Drupal\linked_entity_reference\Plugin\Field\FieldFormatter
 *
 * @FieldFormatter(
 *   id = "linked_entity_reference_label",
 *   label = @Translation("Label"),
 *   description = @Translation("Display the label of the referenced entities."),
 *   field_types = {
 *     "linked_entity_reference"
 *   }
 * )
 */
class LinkedEntityReferenceLabel extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      $label = $entity->label();
      // If the link is to be displayed, display a link.
      /** @var \Drupal\linked_entity_reference\Plugin\Field\FieldType\LinkedEntityReference $item */
      $item = $items[$delta];

      if (!empty($item->uri)) {
        $elements[$delta] = [
          '#type' => 'link',
          '#title' => $label,
          '#url' => $item->getUrl(),
          '#options' => $item->getUrl()->getOptions(),
        ];

        if (!empty($items[$delta]->_attributes)) {
          $elements[$delta]['#options'] += ['attributes' => []];
          $elements[$delta]['#options']['attributes'] += $items[$delta]->_attributes;
          // Unset field item attributes since they have been included in the
          // formatter output and shouldn't be rendered in the field template.
          unset($items[$delta]->_attributes);
        }
      }
      else {
        $elements[$delta] = ['#plain_text' => $label];
      }
      $elements[$delta]['#cache']['tags'] = $entity->getCacheTags();
    }

    return $elements;
  }

}
