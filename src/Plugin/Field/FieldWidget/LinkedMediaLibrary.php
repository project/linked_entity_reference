<?php

namespace Drupal\linked_entity_reference\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media_library\Plugin\Field\FieldWidget\MediaLibraryWidget;

/**
 * Class LinkedMediaLibrary.
 *
 * @package Drupal\linked_entity_reference\Plugin\Field\FieldWidget
 *
 * @FieldWidget(
 *   id = "linked_media_library",
 *   label = @Translation("Media library"),
 *   description = @Translation("Allows you to select items from the media library."),
 *   field_types = {
 *     "linked_media_library"
 *   },
 *   multiple_values = TRUE,
 * )
 */
class LinkedMediaLibrary extends MediaLibraryWidget {

  use LinkedEntityReferenceWidgetTrait;

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    if (!$items->isEmpty()) {
      foreach ($items as $delta => $item) {
        $element['selection'][$delta]['uri'] = $this->getUriWidget($item, FALSE);
      }
    }

    return $element;
  }

}
