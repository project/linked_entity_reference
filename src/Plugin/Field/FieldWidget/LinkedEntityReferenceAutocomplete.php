<?php


namespace Drupal\linked_entity_reference\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class LinkedEntityReferenceAutocomplete
 *
 * @package Drupal\linked_entity_reference\Plugin\Field\FieldWidget
 *
 * @FieldWidget(
 *   id = "linked_entity_reference_autocomplete",
 *   label = @Translation("Autocomplete"),
 *   description = @Translation("An autocomplete text field."),
 *   field_types = {
 *     "linked_entity_reference"
 *   }
 * )
 */
class LinkedEntityReferenceAutocomplete extends EntityReferenceAutocompleteWidget {

  use LinkedEntityReferenceWidgetTrait;

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    if (!empty($items[$delta])) {
      $item = $items[$delta];

      $element['uri'] = $this->getUriWidget($item);
    }

    return $element;
  }

}
