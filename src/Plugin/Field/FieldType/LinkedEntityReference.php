<?php

namespace Drupal\linked_entity_reference\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\MapDataDefinition;
use Drupal\Core\Url;
use Drupal\link\LinkItemInterface;

/**
 * Class LinkedEntityReference.
 *
 * @package Drupal\linked_entity_reference\Plugin\Field\FieldType
 *
 * @FieldType (
 *   id = "linked_entity_reference",
 *   label = @Translation("Linked Entity Reference"),
 *   description = @Translation("Reference to an entity with a link to a arbitrary URL."),
 *   category = @Translation("Reference"),
 *   default_widget = "linked_entity_reference_autocomplete",
 *   default_formatter = "linked_entity_reference_label",
 *   list_class = "\Drupal\Core\Field\EntityReferenceFieldItemList",
 *   constraints = {"LinkType" = {}, "LinkAccess" = {}, "LinkExternalProtocols" = {}, "LinkNotExistingInternal" = {}}
 * )
 */
class LinkedEntityReference extends EntityReferenceItem implements LinkItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
        'link_type' => LinkItemInterface::LINK_GENERIC,
      ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = parent::schema($field_definition);

    $columns['columns']['uri'] = [
      'description' => 'The URI of the link.',
      'type' => 'varchar',
      'length' => 2048,
    ];
    $columns['columns']['options'] = [
      'description' => 'Serialized array of options for the link.',
      'type' => 'blob',
      'size' => 'big',
      'serialize' => TRUE,
    ];

    $columns['indexes']['uri'] = [['uri', 30]];
    return $columns;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    $properties['uri'] = DataDefinition::create('uri')
      ->setLabel(t('URI'));

    $properties['options'] = MapDataDefinition::create()
      ->setLabel(t('Options'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::fieldSettingsForm($form, $form_state);

    $element['link_type'] = [
      '#type' => 'radios',
      '#title' => t('Allowed link type'),
      '#default_value' => $this->getSetting('link_type'),
      '#options' => [
        LinkItemInterface::LINK_INTERNAL => t('Internal links only'),
        LinkItemInterface::LINK_EXTERNAL => t('External links only'),
        LinkItemInterface::LINK_GENERIC => t('Both internal and external links'),
      ],
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function isExternal() {
    return $this->getUrl()->isExternal();
  }

  /**
   * {@inheritdoc}
   */
  public function getUrl() {
    return Url::fromUri($this->uri, (array) $this->options);
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(): ?string {
    return $this->title ?: NULL;
  }

}
