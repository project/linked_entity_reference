<?php

namespace Drupal\linked_entity_reference_slick\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\linked_entity_reference\Plugin\Field\FieldType\LinkedEntityReference;
use Drupal\slick\Plugin\Field\FieldFormatter\SlickMediaFormatter;

/**
 * Class LinkedMediaSlick.
 *
 * @package Drupal\linked_entity_reference\Plugin\Field\FieldFormatter
 *
 * @FieldFormatter(
 *   id = "linked_entity_reference_slick_media",
 *   label = @Translation("Slick Media"),
 *   description = @Translation("Display the referenced entities as a Slick carousel."),
 *   field_types = {
 *     "linked_entity_reference",
 *   },
 *   quickedit = {
 *     "editor" = "disabled"
 *   }
 * )
 */
class LinkedMediaSlick extends SlickMediaFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    if (!$items->isEmpty()) {
      foreach ($items as $delta => $item) {
        if ($item instanceof LinkedEntityReference && !empty($item->uri)) {
          $elements['#build']['items'][$delta]['slide'] = [
            '#type' => 'link',
            '#title' => $elements['#build']['items'][$delta]['slide'],
            '#url' => $item->getUrl(),
            '#options' => $item->getUrl()->getOptions(),
          ];
        }
      }
    }

    return $elements;
  }

}
